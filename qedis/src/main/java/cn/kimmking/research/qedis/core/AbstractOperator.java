package cn.kimmking.research.qedis.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * AbstractOperator.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/11 下午11:36
 */
public abstract class AbstractOperator {

    public static long CURRENT = System.currentTimeMillis();

    public static Map<String, CacheEntry<?>> MAP = new ConcurrentHashMap<>(1024);

    public boolean checkInvalid(String key) {
        CacheEntry<?> entry = getEntry(key);
        if (entry == null || entry.getValue() == null) return true;
        if(entry.getTtl() > 0 && CURRENT - entry.getTs() > entry.getTtl()) {
            System.out.println(String.format("KEY[%s] expire cause CURRENT[%d]-TS[%d] > TTL[%d] ms",
                    key, CURRENT, entry.getTs(), entry.getTtl()));
            MAP.remove(key);
            return true;
        }
        return false;
    }

    public static void updateTs() {
        CURRENT = System.currentTimeMillis();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CacheEntry<T> {
        private T value;
        private long ts; // created timestamp
        private long ttl; // alive ttl
    }

    protected Map<String, CacheEntry<?>> getMap() {
        return MAP;
    }

    protected CacheEntry<?> getEntry(String key) {
        return MAP.get(key);
    }

    protected Object getEntryValue(String key) {
        return getEntry(key).getValue();
    }

    protected void putEntry(String key, CacheEntry<?> entry) {
        MAP.put(key, entry);
    }

}
