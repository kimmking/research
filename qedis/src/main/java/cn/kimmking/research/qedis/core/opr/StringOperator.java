package cn.kimmking.research.qedis.core.opr;

import cn.kimmking.research.qedis.core.AbstractOperator;

/**
 * String operators.
 *
 * @Author : kimmking(kimmking@apache.org)
 * @create 2024/6/11 下午8:04
 */
public class StringOperator extends AbstractOperator {

    public void set(String key, String value) {
        putEntry(key, new CacheEntry<>(value, System.currentTimeMillis(), -1000));
    }

    public String get(String key) {
        if(checkInvalid(key)) return null;
        CacheEntry<String> entry = (CacheEntry<String>) getEntry(key);;
        return entry.getValue();
    }

    public Integer strlen(String key) {
        if(checkInvalid(key)) return 0;
        return this.get(key) == null ? null : this.get(key).length();
    }


}
